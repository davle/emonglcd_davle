#ifndef PowerGraphValues_h
#define PowerGraphValues_h
#include "Arduino.h"
#include "GraphValues.h"

class PowerGraphValues : public GraphValues {
 public:
  void appendFirst(int v, unsigned char minute, unsigned char hour);
  double getValue();
};
#endif

