#include "utility/font_clR4x6.h"
#include "utility/font_clR6x8.h"
#include "utility/font_clB8x14.h"
#include "GraphValues.h"

void drawTopData(double outside, double indoors, int power) { 
  glcd.clear();
  glcd.fillRect(0,0,128,64,0);
  glcd.drawLine(0, 23, 128, 23, WHITE);     // Top horizontal line 

  glcd.setFont(font_clR6x8);      
  glcd.drawString_P(1,  0, PSTR("Inne"));
  glcd.drawString_P(43,0,PSTR("Ute"));
  glcd.drawString_P(85,0,PSTR("El"));

  glcd.setFont(font_clB8x14);      

  char str[10]; 
  dtostrf(indoors,0,1,str);
  glcd.drawString(0,10,str);
  dtostrf(outside,0,1,str);
  glcd.drawString(43,10,str);

  itoa(power,str,10);
  strcat(str,"w");   
  glcd.drawString(85,10,str);

/*******************************************
  glcd.setFont(font_clR4x6);      
  itoa(missed,str,10);
  glcd.drawString(119,0,str);
*******************************************/
}


void drawMaxMin(double max, double min, int hour, int minute, int second) {
  // Draw Max Min
  char str[10]; 
  glcd.setFont(font_clR4x6);      

  itoa(hour,str, 10);
  //leadingZerosDigits(hour, str);
  glcd.drawString(20,25,str);

  itoa(minute,str, 10);
  //leadingZerosDigits(minute, str);
  glcd.drawString(32,25,str);

  itoa(second,str, 10);
  //leadingZerosDigits(second, str);
  glcd.drawString(44,25,str);

  glcd.drawString_P(58, 25, PSTR("Max:"));
  dtostrf(max,0,1,str);
  glcd.drawString(73,25,str);

  glcd.drawString_P(93, 25, PSTR("Min:"));
  dtostrf(min,0,1,str);
  glcd.drawString(108,25,str);
}


/*******************************************
void leadingZerosDigits(int value, char str[]){
  itoa(value,str, 10);
  if (value < 10) {
    str[1] = str[0];
    str[0] = '0';
  }
}
*******************************************/


void drawGraph(GraphValues &values) {
  double max = values.getMax();
  double min = values.getMin();
  int dispAvail = 26;
  int bottomGraphPos = 57;
  double diff = max - min;
  double dotsPerValue = dispAvail / diff;
  int prevY = -1;
  int prevX = -1;

  values.resetGetPos();
  for (int x = 127; x > 16; x--) {
    double v = values.getValue();
    if (v != NULLVALUE) {
      int dots = (int)round((v - min) * dotsPerValue);
      int y = bottomGraphPos - dots;
      if (prevY > 0 and prevX > 0) {
	glcd.drawLine(prevX, prevY, x, y, WHITE);
      } 

      prevY = y;
      prevX = x;
    }
  }

  int minute = values.lastAppendedMinute;
  int hour = values.lastAppendedHour;
  char str[10]; 
  for (int x = 127; x > 16; x--) {
    if (minute == 0) {
      itoa(hour,str,10);
      if (hour == 0 or hour == 3 or hour == 6 or hour == 9 or hour == 12 or hour == 15 or hour == 18 or hour == 21) {
	//glcd.setPixel(x,58,WHITE);
	
	if (hour < 10)
	  glcd.drawString(x-1,59,str);
	else
	  glcd.drawString(x-3,59,str);
      }

      hour--;
      minute = 60;
      if (hour < 0) hour = 23;
    }
    minute -= 10;
  }
  
  double yScale = getYScale(dotsPerValue);
  for (double y = floor(min); y <= max; y+=yScale) {
    if (y > min) drawYScale(y, dotsPerValue, min, true, bottomGraphPos);
  }
}


double getYScale(double dotsPerValue) {
  double yScale;
  if (dotsPerValue < 2) {
    yScale = 4;
  } else if (dotsPerValue < 2.6) {
    yScale = 3;
  } else if (dotsPerValue < 4) {
    yScale = 2.5;
  } else if (dotsPerValue < 5) {
    yScale = 2;
  } else if (dotsPerValue < 6) {
    yScale = 1.5;
  } else if (dotsPerValue < 12) {
    yScale = 1;
  } else if (dotsPerValue < 28) {
    yScale = 0.5;
  } else if (dotsPerValue < 100) {
    yScale = 0.2;
  } else {
    yScale = 0.1;
  }

  return yScale;
}


void drawYScale(double value, double dotsPerValue, double min, bool drawValue, int bottomGraphPos) {
  int dots = (int)round((value - min) * dotsPerValue);
  int y = bottomGraphPos - dots;

  if (drawValue) {
    char str[10]; 
    dtostrf(value,0,1,str); 
    glcd.setFont(font_clR4x6);      
    glcd.drawString(0,y-2,str);
  }

  for (int x = 17; x < 128; x++) {
    if (fmod(x, 2) > 0) glcd.setPixel(x, y, WHITE);
  }
}
