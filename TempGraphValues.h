#ifndef TempGraphValues_h
#define TempGraphValues_h
#include "Arduino.h"
#include "GraphValues.h"

class TempGraphValues : public GraphValues {
 public:
  void appendFirst(double v, unsigned char minute, unsigned char hour);
  double getValue();
};
#endif
