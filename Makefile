# Automatically Generated Makefile by EDE.
# For use with Make for an Arduino project.
#
# DO NOT MODIFY THIS FILE OR YOUR CHANGES MAY BE LOST.
# EDE is the Emacs Development Environment.
# http://cedet.sourceforge.net/ede.shtml

ARDUINO_DIR  = /Applications/Arduino.app/Contents/Resources/Java

TARGET       = emonglcd
ARDUINO_LIBS = MemoryFree Wire Wire/utility RTClib DallasTemperature OneWire GLCD_ST7565 JeeLib

MCU          = atmega328p
F_CPU        = 16000000L
ARDUINO_PORT = /dev/tty.SLAB_USBtoUART
BOARD_TAG    = uno
MAKEFLAGS += --quiet

AVRDUDE_ARD_BAUDRATE = 115200
AVRDUDE_ARD_PROGRAMMER = arduino

include /Applications/Arduino.app/Contents/Resources/Java/Arduino.mk

# End of Makefile
