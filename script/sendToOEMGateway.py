import datetime
import socket
import struct
import json, requests
import time

# Define some values
emoncmsUrl = 'http://emoncms.org/feed/list.json'
apikey ='xxx'
HOST = 'raspberrypi.local'
PORT = 50011



####
# Converts byte string to integer. Use Little-endian byte order.
####
def leUnpack(byte):
    return sum([ ord(b) << (8 * i) for i, b in enumerate(byte) ])


####
# Get payload for realtime temperature.
####
def getPayloadTemp(int1, int2):
    payloadByteString = struct.pack("<Bhh",0, int1, int2)

    payload = ""
    for c in payloadByteString:
        payload += str(leUnpack(c))
        payload += ", "

    payload += " s\r\n"
    return payload


####
# Get payload time.
####
def getPayloadTime(hour, minute, second):
    payloadByteString = struct.pack("<BBBB",1, hour, minute, second)

    payload = ""
    for c in payloadByteString:
        payload += str(leUnpack(c))
        payload += ", "

    payload += " s\r\n"
    return payload


####
# Get values from emoncms web service.
####
def getValuesFromEmoncms():
    params = dict(apikey=apikey)

    resp = None    
    try:
    	resp = requests.get(url=emoncmsUrl, params=params)
    	
    except Exception, e:
	currentTime = time.strftime("%Y-%m-%d %H:%M:%S")
	print currentTime, " Failed with error", e 	    

    if resp:
    	data = json.loads(resp.content)
	return data
    
    return None


def sendTime():
    localtime = time.localtime(time.time())
    payload = getPayloadTime(localtime.tm_hour, localtime.tm_min, localtime.tm_sec)
    actualSend(payload)

    
def sendValues():
    data = getValuesFromEmoncms()
    if data:
    	outsideTemp = float(data[0]['value'])
    	outsideTemp = int(outsideTemp * 10)

    	indoorsTemp = float(data[6]['value'])
    	indoorsTemp = int(indoorsTemp * 10)
    	payload = getPayloadTemp(outsideTemp, indoorsTemp)
    	actualSend(payload)

                
def actualSend(payload):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)    
    try:
        s.connect((HOST, PORT))
    except Exception, e:
        print("Something's wrong with %s. Exception type is %s" % (HOST, e))

    currentTime = time.strftime("%Y-%m-%d %H:%M:%S")
    print currentTime, " Sending: ", payload
    s.send(payload)
    s.close()

    
def mainLoop(seconds):
    sendTime()
    time.sleep(10)
    while True:
        sendValues()
        time.sleep(seconds)
    

mainLoop(60)
