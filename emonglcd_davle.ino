// Libraries in the standard arduino libraries folder:
//
//	- OneWire library	http://www.pjrc.com/teensy/td_libs_OneWire.html
//	- DallasTemperature	http://download.milesburton.com/Arduino/MaximTemperature
//                           or https://github.com/milesburton/Arduino-Temperature-Control-Library
//	- JeeLib		https://github.com/jcw/jeelib
//	- RTClib		https://github.com/jcw/rtclib
//	- GLCD_ST7565		https://github.com/jcw/glcdlib
#include "GraphValues.h"
#include "TempGraphValues.h"
//#include <PowerGraphValues.h>
#include <JeeLib.h>
#include <GLCD_ST7565.h>
#include <avr/pgmspace.h>
#include <OneWire.h>		    // http://www.pjrc.com/teensy/td_libs_OneWire.html
#include <DallasTemperature.h>      // http://download.milesburton.com/Arduino/MaximTemperature/ (3.7.2 Beta needed for Arduino 1.0)
#include <Wire.h>                   // Part of Arduino libraries - needed for RTClib
//#include <MemoryFree.h>
#include <RTClib.h>                 // Real time clock (RTC) - used for software RTC to reset kWh counters at midnight

//--------------------------------------------------------------------------------------------
// Settings
//--------------------------------------------------------------------------------------------
#define MYNODE 21            // Should be unique on network, node ID 30 reserved for base station
#define RF_freq RF12_868MHZ     // frequency - match to same frequency as RFM12B module (change to 868Mhz or 915Mhz if appropriate)
#define group 210 
#define ONE_WIRE_BUS 5              // temperature sensor connection - hard wired 

unsigned long fast_update, slow_update;

// 0 outside, 1 indoors
int displayGraph;
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);
double temp;
GLCD_ST7565 glcd;
TempGraphValues outside;
TempGraphValues indoor;
//PowerGraphValues power;
RTC_Millis RTC;

//---------------------------------------------------
// Data structures for transfering data between units
//---------------------------------------------------

// Command 0 does nothing
// Command 1 fetches time
typedef struct { int temperature, command; 
} PayloadGLCD;
PayloadGLCD emonglcd;

// Type 0 from raspberry pi
typedef struct {
  unsigned char destNode;
  int type, temp1, temp2;
} PayloadPiTemp;
PayloadPiTemp piTemp;  

// Type 1 from raspberry pi
typedef struct {
  unsigned char destNode;
  int type, hour, minute, second;
} PayloadPiTime;

// Type 2 from raspberry pi
typedef struct { 
  unsigned char destNode;
  int type, power;
} PayloadPiPower;
PayloadPiPower piPower;  


const int greenLED=6;               // Green tri-color LED
const int redLED=9;                 // Red tri-color LED
const int LDRpin=4;    		    // analog pin of onboard
				    // lightsensor
const int enterswitchpin=15;        // digital pin of enter switch - low when pressed
int cval_use;
boolean enterSwitchState;
boolean appendingData;
boolean anyDataReceived;
boolean anyPowerDataReceived;
boolean anyTimeReceived;


//--------------------------------------------------------------------------------------------
// Setup
//--------------------------------------------------------------------------------------------
void setup()
{
  delay(500); 				   //wait for power to settle before firing up the RF
  rf12_initialize(MYNODE, RF_freq,group);
  delay(100);				   //wait for RF to settle befor turning on display
  glcd.begin(0x19);
  glcd.backLight(200);
  
  sensors.begin();                         // start up the DS18B20 temp sensor onboard  
  sensors.requestTemperatures();
  temp = (sensors.getTempCByIndex(0));     // get inital temperture reading

  pinMode(greenLED, OUTPUT); 
  pinMode(redLED, OUTPUT); 
  pinMode(enterswitchpin, INPUT);

  if (Serial) {
    Serial.begin(9600);
    Serial.println("emonGLCD"); 
    delay(100);
  }

  // Begin with outside
  displayGraph = 0;

  // Request new time
  emonglcd.command = 1;
  rf12_sendNow(0, &emonglcd, sizeof emonglcd);
  rf12_sendWait(2);  
  emonglcd.command = 0;
}

//--------------------------------------------------------------------------------------------
// Loop
//--------------------------------------------------------------------------------------------
void loop() {
  // Switch display graph on enter key
  if (digitalRead(enterswitchpin) == 1) {
    if (!enterSwitchState) {
      displayGraph++;
      if (displayGraph > 1) displayGraph = 0;
      enterSwitchState = true;
    }
  } else {
    enterSwitchState = false;
  }
  
  if (rf12_recvDone()) {
    if (rf12_crc == 0 && (rf12_hdr & RF12_HDR_CTL) == 0) {
      // and no rf errors
      int node_id = (rf12_hdr & 0x1F);
      if (node_id == 10) { // Deprecated now when power is sent over wifi
	//Assuming 10 is the emonTx NodeID
	//emonth = *(PayloadTH*) rf12_data;
	//anyPowerDataReceived = true;
	//digitalWrite(greenLED,HIGH);
	//delay(100);
	//digitalWrite(greenLED,LOW);
      } else if (node_id == 15) {
	if (rf12_data[1] == 0) {
	  piTemp = *(PayloadPiTemp*) rf12_data;
	  anyDataReceived = true;
	} else if (rf12_data[1] == 1) {
	  PayloadPiTime piTime = *(PayloadPiTime*) rf12_data;
	  Serial.println(piTime.destNode);
	  if (piTime.destNode == 21) {
	    RTC.adjust(DateTime(2015, 1, 1, piTime.hour, piTime.minute, piTime.second));
	    anyTimeReceived = true;
	  }
	} else if (rf12_data[1] == 2) {
	  piPower = *(PayloadPiPower*) rf12_data;
	  anyPowerDataReceived = true;
	}
	
	//digitalWrite(redLED,HIGH);
	//delay(50);
	//digitalWrite(redLED,LOW);
      } 
    }    
  }


  //--------------------------------------------------------------------------------------------
  // Display update every 200ms
  //--------------------------------------------------------------------------------------------
  if ((millis()-fast_update)>200) {
    fast_update = millis();
    cval_use = cval_use + (piPower.power - cval_use)*0.50;        //smooth transitions

    // Get time
    DateTime now = RTC.now();
    int hour = now.hour();
    int minute = now.minute();
    int second = now.second();

    // Append values every 10 minutes once data actually was received
    //if (anyDataReceived and (second == 0)) {
    if (anyDataReceived and anyTimeReceived and (minute == 0 or minute == 10 or minute == 20 or minute == 30 or minute == 40 or minute == 50)) {
      if (!appendingData) {
	//digitalWrite(redLED,HIGH);
	//delay(100);
	//digitalWrite(redLED,LOW);
	outside.appendFirst(piTemp.temp1/10.0, minute, hour);
	indoor.appendFirst(piTemp.temp2/10.0, minute, hour);
	//if (anyPowerDataReceived) power.appendFirst(emonth.power, minute, hour);
	appendingData = true;
      }
    } else {
      appendingData = false;
    }
    
    // Draw top data, max min and graph
    double out = piTemp.temp1/10.0;
    double in = piTemp.temp2/10.0;
    drawTopData(out, in, cval_use);

    if (displayGraph == 0) {
      drawMaxMin(outside.getMax(), outside.getMin(), hour, minute, second);
      drawGraph(outside);
    } else if (displayGraph == 1) {
      drawMaxMin(indoor.getMax(), indoor.getMin(), hour, minute, second);
      drawGraph(indoor);
    }
    glcd.refresh();

    int LDR = analogRead(LDRpin);                     // Read the LDR Value so we can work out the light level in the room.
    int LDRbacklight = map(LDR, 0, 1023, 30, 250);    // Map the data from the LDR from 0-1023 (Max seen 1000) to var GLCDbrightness min/max
    LDRbacklight = constrain(LDRbacklight, 0, 255);   // Constrain the value to make sure its a PWM value 0-255
    //if ((hour > 22) ||  (hour < 5)) glcd.backLight(0); else glcd.backLight(LDRbacklight);  
    glcd.backLight(LDRbacklight);  
    //glcd.backLight(0);  
  } 
  
  //--------------------------------------------------------------------------------------------
  // Send emonglcd temp every 60 seconds
  //--------------------------------------------------------------------------------------------
  if ((millis()-slow_update)>60000) {
    slow_update = millis();

    sensors.requestTemperatures();
    temp = (sensors.getTempCByIndex(0));
    
    // For some reason it is off 0.75 deg
    //temp = temp - 0.75;

    // set emonglcd payload
    emonglcd.temperature = (int) (temp * 100);

    // send temperature data via RFM12B using new rf12_sendNow wrapper -glynhudson
    rf12_sendNow(0, &emonglcd, sizeof emonglcd);
    rf12_sendWait(2);  
  }
}
