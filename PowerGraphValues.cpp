#include "Arduino.h"
#include "PowerGraphValues.h"

/**
 * Get value and advance pos one step forward, returns NULLVALUE when out of bounds.
 */
double PowerGraphValues::getValue() {
  // Nothing added, return NULLVALUE
  if (firstValue == NULLVALUE) {
    return firstValue;
  }

  // First get, set lastGet and return firstValue
  if (lastGetValue == NULLVALUE) {
    lastGetValue = firstValue;
    return firstValue;
  }
  
  // Get last value and append byte value (diff) to that
  if (getPos < nrOfValues) {
    byte byteValue = values[getPos];
    getPos++;
    if (byteValue != BYTENULLVALUE) {
      int value = lastGetValue + (byteValue * 50);
      lastGetValue = value; 
      return value;
    }
  }
  
  return NULLVALUE;
}


/**
 * Append value first, shift all values one step backwards.
 */
void PowerGraphValues::appendFirst(int v, unsigned char minute, unsigned char hour) {
  lastAppendedMinute = minute;
  lastAppendedHour = hour;

  for (int i = nrOfValues - 2; i >= 0; i--) {
    values[i+1] = values[i];
  }
  
  int oldFirstValue = firstValue;
  firstValue = v;
  if (oldFirstValue != NULLVALUE) values[0] = round((oldFirstValue - firstValue) / 50);

  // Calculate max and min
  max=-50000;
  min=50000;
  resetGetPos();
  double value = getValue();
  while(value != NULLVALUE) {
    if (value > max) max = value;
    if (value < min) min = value;
    value = getValue();
  }
}
