#include "Arduino.h"
#include "GraphValues.h"

GraphValues::GraphValues() {
  lastGetValue = NULLVALUE;
  firstValue = NULLVALUE;

  // Reset all values to 0.
  for (int i = 0; i < nrOfValues; i++) {
    values[i] = 0;
  }
}

GraphValues::~GraphValues(){/*nothing to destruct*/}


/**
 * Dump raw values, for testing.
 */
void GraphValues::dumpRawValues() {
  if (Serial) {
    Serial.println("Dumping raw values");
    for (int i = 0; i < 10; i++) {
      Serial.print(i);
      Serial.print(" ");
      Serial.println(values[i]);
    }
  }
}


/**
 * Reset get pos to 0.
 */
void GraphValues::resetGetPos() {
  getPos = 0;
  lastGetValue = NULLVALUE;
}


/**
 * Get value and advance pos one step forward, returns NULLVALUE when out of bounds.
 */
double GraphValues::getValue() {
  return 0;
}


/**
 * Current get pos.
 */
int GraphValues::currentGetPos() {
  return getPos;
}


/**
 * Get max value.
 */
double GraphValues::getMax() {
  return max;
}


/**
 * Get min value.
 */
double GraphValues::getMin() {
  return min;
}
