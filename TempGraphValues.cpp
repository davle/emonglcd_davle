#include "Arduino.h"
#include "TempGraphValues.h"

/**
 * Get value and advance pos one step forward, returns NULLVALUE when out of bounds.
 */
double TempGraphValues::getValue() {
  // Nothing added, return NULLVALUE
  if (firstValue == NULLVALUE) {
    return firstValue;
  }

  // First get, set lastGet and return firstValue
  if (lastGetValue == NULLVALUE) {
    lastGetValue = firstValue;
    return firstValue / 10.0;
  }
  
  // Get last value and append byte value (diff) to that
  if (getPos < nrOfValues) {
    byte byteValue = values[getPos];
    getPos++;
    if (byteValue) {
      int value = lastGetValue + (byteValue - 128);
      lastGetValue = value; 
      return value / 10.0;
    }
  }
  
  return NULLVALUE;
}


/**
 * Append value first, shift all values one step backwards.
 */
void TempGraphValues::appendFirst(double v, unsigned char minute, unsigned char hour) {
  lastAppendedMinute = minute;
  lastAppendedHour = hour;

  for (int i = nrOfValues - 2; i >= 0; i--) {
    values[i+1] = values[i];
  }
  
  int oldFirstValue = firstValue;
  firstValue = int(v * 10);
  if (oldFirstValue != NULLVALUE) values[0] = (oldFirstValue - firstValue) + 128;

  // Calculate max and min
  max=-10000;
  min=10000;
  resetGetPos();
  double value = getValue();
  while(value != NULLVALUE) {
    if (value > max) max = value;
    if (value < min) min = value;
    value = getValue();
  }
}
