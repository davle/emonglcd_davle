#ifndef GraphValues_h
#define GraphValues_h
#include "Arduino.h"
#define NULLVALUE -32768 // Min int
#define BYTENULLVALUE 255

const int nrOfValues=110;

class GraphValues {
 public:
  GraphValues();
  ~GraphValues();
  void dumpValues();
  void resetGetPos();
  int currentGetPos();
  double getMax();
  double getMin();
  unsigned char lastAppendedMinute;
  unsigned char lastAppendedHour;
  void dumpRawValues();
  virtual double getValue();
 protected:
  byte values[nrOfValues];
  int getPos;
  int lastGetValue;
  int firstValue;
  double max;
  double min;
};
#endif
